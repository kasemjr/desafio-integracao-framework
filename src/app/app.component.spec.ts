import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { AppComponent } from './app.component';
import { Lancamento } from './models/lancamento';
import { CategoriasService } from './services/categorias.service';
import { LancamentosService } from './services/lancamentos.service';
import { Categoria } from './models/categoria';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let lancamentosServiceMock: jasmine.SpyObj<LancamentosService>;
  let categoriasServiceMock: jasmine.SpyObj<CategoriasService>;

  beforeEach(async(() => {
    lancamentosServiceMock = jasmine.createSpyObj('lancamentosService', [
      'carregarLancamentos',
      'consolidarLancamentos'
    ]);

    categoriasServiceMock = jasmine.createSpyObj('lancamentosService', [
      'carregarCategorias'
    ]);

    TestBed
      .configureTestingModule({
        declarations: [AppComponent],
        providers: [
          { provide: LancamentosService, useValue: lancamentosServiceMock },
          { provide: CategoriasService, useValue: categoriasServiceMock }
        ],
        schemas: [
          CUSTOM_ELEMENTS_SCHEMA
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('deve carregar a lista de lançamentos ao inicializar o componente', () => {

    lancamentosServiceMock.carregarLancamentos.and.returnValue(of());
    lancamentosServiceMock.consolidarLancamentos.and.returnValue(of());
    categoriasServiceMock.carregarCategorias.and.returnValue(of());

    fixture.detectChanges();

    expect(component.lancamentos$).toBeInstanceOf(Observable);
    expect(lancamentosServiceMock.carregarLancamentos).toHaveBeenCalled();

  });

  it('deve carregar a lista de categorias ao inicializar o componente', () => {

    lancamentosServiceMock.carregarLancamentos.and.returnValue(of());
    lancamentosServiceMock.consolidarLancamentos.and.returnValue(of());
    categoriasServiceMock.carregarCategorias.and.returnValue(of());

    fixture.detectChanges();

    expect(component.categorias$).toBeInstanceOf(Observable);
    expect(categoriasServiceMock.carregarCategorias).toHaveBeenCalled();

  });

  it('deve converter a lista de categorias para um mapa de categorias', () => {

    const categoriaTransporte = new Categoria();
    categoriaTransporte.id = 1;
    categoriaTransporte.nome = 'Transporte';

    const categoriaRestaurantes = new Categoria();
    categoriaRestaurantes.id = 2;
    categoriaRestaurantes.nome = 'Restaurantes';

    const categorias: Categoria[] = [
      categoriaTransporte,
      categoriaRestaurantes
    ];

    lancamentosServiceMock.carregarLancamentos.and.returnValue(of());
    lancamentosServiceMock.consolidarLancamentos.and.returnValue(of());
    categoriasServiceMock.carregarCategorias.and.returnValue(of(categorias));

    fixture.detectChanges();

    component.categorias$.subscribe(categorias => {

      expect(categorias.get(categoriaTransporte.id)).toEqual(categoriaTransporte);
      expect(categorias.get(categoriaRestaurantes.id)).toEqual(categoriaRestaurantes);

    });

  });

  it('deve consolidar os lançamentos ao inicializar componente', () => {

    const lancamentos: Lancamento[] = [new Lancamento()];

    lancamentosServiceMock.carregarLancamentos.and.returnValue(of(lancamentos));
    lancamentosServiceMock.consolidarLancamentos.and.returnValue(of());
    categoriasServiceMock.carregarCategorias.and.returnValue(of());

    fixture.detectChanges();

    expect(component.consolidados$).toBeInstanceOf(Observable);
    expect(lancamentosServiceMock.consolidarLancamentos).toHaveBeenCalledWith(lancamentos);

  });

});
