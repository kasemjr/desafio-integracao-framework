/**
 * Classe que representa os dados de uma determinada categoria de lançamento
 */
export class Categoria {
  id: number;
  nome: string;
}
