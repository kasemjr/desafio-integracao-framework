/**
 * Classe que representa os dados de um determinado lançamento do cartão
 */
export class Lancamento {

  id: number;
  valor: number;
  origem: string;
  categoria: number;
  mes: number;

}
