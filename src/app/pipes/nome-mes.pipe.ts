import { Pipe, PipeTransform, Inject, LOCALE_ID } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'nomeMes'
})
export class NomeMesPipe extends DatePipe {

  constructor(@Inject(LOCALE_ID) private _locale: string) {
    super(_locale);
  }

  transform(mes: number): string {
    const data = new Date();
    data.setDate(1);
    data.setMonth(mes - 1);

    let nomeMes: string = super.transform(data, 'MMMM');
    nomeMes = nomeMes[0].toUpperCase() + nomeMes.slice(1);

    return nomeMes;
  }

}
