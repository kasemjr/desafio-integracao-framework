import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { CategoriasService } from './categorias.service';
import { Categoria } from '../models/categoria';

describe('CategoriasService', () => {
  let service: CategoriasService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CategoriasService]
    });
    service = TestBed.get(CategoriasService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('deve ser criado', () => {
    expect(service).toBeTruthy();
  });

  it('deve recuperar a lista de categorias de lançamento', () => {

    const categoriasJson: any[] = [
      { "id": 1, "nome": "Transporte" }
    ];

    service
      .carregarCategorias()
      .subscribe(categorias => {

        expect(categorias.length).toBe(1);

      });

    const request = httpTestingController.expectOne(`${environment.api_endpoint}/categorias`);

    expect(request.request.method).toBe('GET');

    request.flush(categoriasJson);

  });

  it('as categorias devem ser instancias do modelo Categoria', () => {

    const categoriasJson: any[] = [
      { "id": 1, "nome": "Transporte" }
    ];

    service
      .carregarCategorias()
      .subscribe(categorias => {

        categorias.forEach(categoria => {
          expect(categoria).toBeInstanceOf(Categoria);
        });

      });

    const request = httpTestingController.expectOne(`${environment.api_endpoint}/categorias`);

    expect(request.request.method).toBe('GET');

    request.flush(categoriasJson);

  });

  it('deve converter dos dados do lançamento corretamente', () => {

    const json: any = { "id": 1, "nome": "Transporte" };

    const categoriasJson: any[] = [json];

    service
      .carregarCategorias()
      .subscribe(categorias => {

        const categoria = categorias[0];

        expect(categoria.id).toEqual(json.id);
        expect(categoria.nome).toEqual(json.nome);

      });

    const request = httpTestingController.expectOne(`${environment.api_endpoint}/categorias`);

    expect(request.request.method).toBe('GET');

    request.flush(categoriasJson);

  })

});
