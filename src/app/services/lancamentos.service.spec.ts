import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { LancamentosService } from './lancamentos.service';
import { Lancamento } from '../models/lancamento';

describe('LancamentosService', () => {
  let service: LancamentosService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LancamentosService]
    });
    service = TestBed.get(LancamentosService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  const gerarLancamento = (
    id: number,
    mes: number,
    origem: string,
    valor: number,
    categoria: number) => {

    const lancamento = new Lancamento();
    lancamento.id = id;
    lancamento.mes = mes;
    lancamento.categoria = categoria;
    lancamento.valor = valor;
    lancamento.origem = origem;

    return lancamento;
  };

  it('deve ser criado', () => {
    expect(service).toBeTruthy();
  });

  it('deve recuperar a lista de lançamentos do cartão', () => {

    const lancamentosJson: any[] = [
      {
        "id": 1,
        "valor": 13.3,
        "origem": "Uber",
        "categoria": 1,
        "mes_lancamento": 1
      }
    ];

    service
      .carregarLancamentos()
      .subscribe(lancamentos => {

        expect(lancamentos.length).toBe(1);

      });

    const request = httpTestingController.expectOne(`${environment.api_endpoint}/lancamentos`);

    expect(request.request.method).toBe('GET');

    request.flush(lancamentosJson);

  });

  it('os lançamentos devem ser instancias do modelo Lancamento', () => {

    const lancamentosJson: any[] = [
      {
        "id": 1,
        "valor": 13.3,
        "origem": "Uber",
        "categoria": 1,
        "mes_lancamento": 1
      }
    ];

    service
      .carregarLancamentos()
      .subscribe(lancamentos => {

        lancamentos.forEach(lancamento => {
          expect(lancamento).toBeInstanceOf(Lancamento);
        });

      });

    const request = httpTestingController.expectOne(`${environment.api_endpoint}/lancamentos`);

    expect(request.request.method).toBe('GET');

    request.flush(lancamentosJson);

  });

  it('deve converter dos dados do lançamento corretamente', () => {

    const jsonLancamento: any = {
      "id": 1,
      "valor": 13.3,
      "origem": "Uber",
      "categoria": 1,
      "mes_lancamento": 1
    };

    const lancamentosJson: any[] = [jsonLancamento];

    service
      .carregarLancamentos()
      .subscribe(lancamentos => {

        const lancamento = lancamentos[0];

        expect(lancamento.id).toEqual(jsonLancamento.id);
        expect(lancamento.valor).toEqual(jsonLancamento.valor);
        expect(lancamento.origem).toEqual(jsonLancamento.origem);
        expect(lancamento.categoria).toEqual(jsonLancamento.categoria);
        expect(lancamento.mes).toEqual(jsonLancamento.mes_lancamento);

      });

    const request = httpTestingController.expectOne(`${environment.api_endpoint}/lancamentos`);

    expect(request.request.method).toBe('GET');

    request.flush(lancamentosJson);

  });

  it('deve consolidar os lançamentos por mês', () => {

    const lancamentos: Lancamento[] = [
      gerarLancamento(1, 1, 'Uber', 10, 1),
      gerarLancamento(2, 1, 'Uber', 10, 1),
      gerarLancamento(3, 2, 'Uber', 30, 1)
    ];

    service
      .consolidarLancamentos(lancamentos)
      .subscribe(consolidado => {

        expect(consolidado.length).toEqual(2);

        const total_m1c1 = consolidado.find(c => c.mes == 1 && c.categoria == 1)?.total;
        const total_m2c1 = consolidado.find(c => c.mes == 2 && c.categoria == 1)?.total;

        expect(total_m1c1).toEqual(20);
        expect(total_m2c1).toEqual(30);

      });

  });

  it('deve consolidar os lançamentos por mês e categoria', () => {

    const lancamentos: Lancamento[] = [
      gerarLancamento(1, 1, 'Uber', 10, 1),
      gerarLancamento(2, 1, 'Outback', 20, 2),
      gerarLancamento(3, 2, 'Uber', 30, 1),
    ];

    service
      .consolidarLancamentos(lancamentos)
      .subscribe(consolidado => {

        expect(consolidado.length).toEqual(3);

        const total_m1c1 = consolidado.find(c => c.mes == 1 && c.categoria == 1)?.total;
        expect(total_m1c1).toEqual(10);

        const total_m1c2 = consolidado.find(c => c.mes == 1 && c.categoria == 2)?.total;
        expect(total_m1c2).toEqual(20);

        const total_m2c1 = consolidado.find(c => c.mes == 2 && c.categoria == 1)?.total;
        expect(total_m2c1).toEqual(30);

      });

  });

  it('deve corrigir o ponto flutuante no total consolidado', () => {

    const lancamentos: Lancamento[] = [
      gerarLancamento(1, 1, 'Uber', 10, 1),
      gerarLancamento(2, 1, 'Uber', 10.56, 1)
    ];

    service
      .consolidarLancamentos(lancamentos)
      .subscribe(consolidado => {

        const total_m1c1 = consolidado.find(c => c.mes == 1 && c.categoria == 1)?.total;
        expect(total_m1c1).toEqual(20.56);

      });

  });

  it('deve ordenar os dados consolidados por mês', () => {

    const lancamentos: Lancamento[] = [
      gerarLancamento(1, 2, 'Uber', 20, 2),
      gerarLancamento(2, 1, 'Uber', 10, 1)
    ];

    service
      .consolidarLancamentos(lancamentos)
      .subscribe(consolidado => {

        expect(consolidado[0].mes).toEqual(1);
        expect(consolidado[1].mes).toEqual(2);

      });

  });

});
