import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Categoria } from '../models/categoria';
import { Lancamento } from '../models/lancamento';
import { NomeMesPipe } from '../pipes/nome-mes.pipe';
import { TabelaLancamentosComponent } from './tabela-lancamentos.component';

registerLocaleData(localePt, 'pt');

describe('TabelaLancamentosComponent', () => {
  let component: TabelaLancamentosComponent;
  let fixture: ComponentFixture<TabelaLancamentosComponent>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [
          TabelaLancamentosComponent,
          NomeMesPipe
        ],
        providers: [
          { provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL' },
          { provide: LOCALE_ID, useValue: 'pt' }
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaLancamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const gerarLancamento = (
    id: number,
    mes: number,
    origem: string,
    valor: number,
    categoria: number) => {

    const lancamento = new Lancamento();
    lancamento.id = id;
    lancamento.mes = mes;
    lancamento.categoria = categoria;
    lancamento.valor = valor;
    lancamento.origem = origem;

    return lancamento;
  };

  const gerarCategoria = (id: number, nome: string): Categoria => {
    const categoria = new Categoria();
    categoria.id = id;
    categoria.nome = nome;

    return categoria;
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve exibir a origem do lançamento', () => {

    const lancamentoStub: Lancamento = gerarLancamento(1, 1, 'Comércio Teste', 1234.56, 1);

    component.categorias = new Map<number, Categoria>();
    component.lancamentos = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.lancamento-origem'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual(lancamentoStub.origem);

  });

  it('deve exibir o valor do lançamento formatado', () => {

    const categoriaStub: Categoria = gerarCategoria(1, 'Transporte');
    const lancamentoStub: Lancamento = gerarLancamento(1, 1, 'Comércio Teste', 1234.56, 1);

    component.categorias = new Map<number, Categoria>();
    component.lancamentos = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.lancamento-valor'))
      .nativeElement
      .innerHTML
      .replace('&nbsp;', ' ')
      .trim();

    expect(textoCelula).toEqual('R$ 1.234,56');

  });

  it('deve exibir o nome do mês do lançamento', () => {

    const categoriaStub: Categoria = gerarCategoria(1, 'Transporte');
    const lancamentoStub: Lancamento = gerarLancamento(1, 1, 'Comércio Teste', 1234.56, 1);

    component.categorias = new Map<number, Categoria>();
    component.lancamentos = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.lancamento-mes'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual('Janeiro');

  });

  it('deve exibir o nome da categoria do lançamento', () => {

    const categoriaStub: Categoria = gerarCategoria(1, 'Transporte');
    const categoriasMaps: Map<number, Categoria> = new Map<number, Categoria>()
    categoriasMaps.set(categoriaStub.id, categoriaStub);

    const lancamentoStub: Lancamento = gerarLancamento(1, 1, 'Comércio Teste', 1234.56, 1);

    component.categorias = categoriasMaps;
    component.lancamentos = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.lancamento-categoria'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual(categoriaStub.nome);

  });

  it('deve exibir "Não categorizado" quando a categoria do lançamento não for encontrada', () => {

    const lancamentoStub: Lancamento = gerarLancamento(1, 1, 'Comércio Teste', 1234.56, 100);

    component.categorias = new Map<number, Categoria>();
    component.lancamentos = [lancamentoStub];

    fixture.detectChanges();

    const textoCelula = fixture.debugElement
      .query(By.css('.lancamento-categoria'))
      .nativeElement
      .textContent
      .trim();

    expect(textoCelula).toEqual('Não categorizado');

  });

});
