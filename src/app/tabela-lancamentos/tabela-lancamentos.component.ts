import { Component, Input } from '@angular/core';
import { Categoria } from '../models/categoria';
import { Lancamento } from '../models/lancamento';

@Component({
  selector: 'app-tabela-lancamentos',
  templateUrl: './tabela-lancamentos.component.html'
})
export class TabelaLancamentosComponent {

  @Input() categorias: ReadonlyMap<number, Categoria>;
  @Input() lancamentos: Lancamento[];

}
